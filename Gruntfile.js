/*!
 * Front-End Project Gruntfile
 * http://gotoplana.eu
 * Copyright 2015 PlanA, Co.
 * Licensed under MIT
 */

module.exports = function (grunt) {
  'use strict';

  // Force use of Unix newlines
  grunt.util.linefeed = '\n';

  // Project configuration.
  grunt.initConfig({

    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*!\n' +
            ' * Copyright 2014-<%= grunt.template.today("yyyy") %> <%= pkg.author %>\n' +
            ' * Licensed under the <%= bootstrapConfig.pkg.license %> license\n' +
            ' */\n',

    // Configurations.
    bootstrapConfig: {
      pkg: grunt.file.readJSON('bower_components/bootstrap/package.json'),
      banner: '/*!\n' +
              ' * Bootstrap v<%= bootstrapConfig.pkg.version %> (<%= bootstrapConfig.pkg.homepage %>)\n' +
              ' * Copyright 2011-<%= grunt.template.today("yyyy") %> <%= bootstrapConfig.pkg.author %>\n' +
              ' * Licensed under the <%= bootstrapConfig.pkg.license %> license\n' +
              ' */\n',
      configBridge: grunt.file.readJSON('bower_components/bootstrap/grunt/configBridge.json', { encoding: 'utf8' })
    },
    config: require('./Gruntfile.config.js'),

    // Task configuration.
    clean: {
      dist:  'dist',
      js:    'dist/javascripts',
      css:   'dist/stylesheets',
      img:   'dist/images',
      fonts: 'dist/fonts'
    },

    concat: {
      bootstrap: {
        options: {
          banner: '<%= bootstrapConfig.banner %>\n<%= bootstrapConfig.configBridge.config.jqueryCheck %>\n\n<%= bootstrapConfig.configBridge.config.jqueryVersionCheck %>',
          stripBanners: false
        },
        files: [{
          src: '<%= config.components.bootstrap %>',
          dest: 'dist/javascripts/bootstrap.js'
        }]
      },

      components: {
        files: {
          src: '<%= config.components.external %>',
          dest: 'dist/javascripts/components.js'
        }
      },

      application: {
        options: {
          banner: '<%= banner %>\n',
        },
        files: [{
          expand: true,
          src: ['js/*.js', 'js/application.js'],
          dest: 'dist/javascripts/application.js'
        }]
      }
    },

    copy: {
      jquery: {
        files: [{
          expand: true,
          src: 'bower_components/jquery/dist',
          dest: '<%= config.dest.js %>'
        }]
      }
    },

    less: {
      bootstrap: {
        options: {
          paths: [
            'less/bootstrap',
            'bower_components/bootstrap/less'
          ]
        },
        files: {
          'dist/stylesheets/bootstrap.css': 'less/bootstrap/bootstrap.less',
          'dist/stylesheets/bootstrap-theme.css': 'bower_componets/less/bootstrap/theme.less'
        }
      },

      application: {
        options: {
          paths: function(source) {

            // Processing path to array
            // ['bower_components', '<%= component %>', 'less', ...] or
            // ['less', 'vendor', '<%= component %>']
            var sourceDirList = [];
            var sourceIterator = path.dirname(source);
            while (sourceIterator) {
              sourceDirList = [path.basename(sourceIterator)].concat();
              sourceIterator = path.dirname(sourceIterator);
            }

            var components = grunt.file.expand('bower_components/*');
            var paths = [
              'less',
              'bower_componets'
            ];

            return paths;
          }
        },
        files: {
          expand: true,
          cwd: 'less',
          src: '*.less',
          dest: 'dist/stylesheets'
        }
      }
    },

    ect: {
      dist: {
        files: [{
          expand: true,
          cwd: 'ect',
          src: '**/*.html.ect',
          dest: 'dist',
          ext: '',
          extDot: 'last'
        }]
      }
    },

    processhtml: {
      debug: {
        files: [{
          expand: true,
          cwd: 'dist',
          src: '**/*.html',
          dest: 'dist'
        }]
      },

      release: '<%= processhtml.debug %>',
    }
  });

  // These plugins provide necessary tasks.
  require('load-grunt-tasks')(grunt, { scope: 'devDependencies' });
  require('time-grunt')(grunt);

  grunt.registerTask('default', ['ect']);
};
