/*!
 * Front-End Project configuration file for Grunt
 * http://gotoplana.eu
 * Copyright 2015 PlanA, Co.
 * Licensed under MIT
 */

module.exports = {
  components: {
    bootstrap: [
      'transition.js',
      'alert.js',
      'button.js',
      'carousel.js',
      'collapse.js',
      'dropdown.js',
      'modal.js',
      'tooltip.js',
      'popover.js',
      'scrollspy.js',
      'tab.js',
      'affix.js'
    ],

    external: [
      // external scripts and plugins goes here
      // base path is bower_components/
    ]
  }
};
